package by.zapotylok.xml.command;


import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import by.zapotylok.xml.entity.Gem;
import by.zapotylok.xml.entity.SyntheticIndustry;
import by.zapotylok.xml.entity.SyntheticJewel;
import by.zapotylok.xml.exception.LogicException;
import by.zapotylok.xml.factory.GemsBuilderFactory;
import by.zapotylok.xml.parser.GemDOMParser;

public class FindAllDOM implements ActionCommand {

	private static final Logger LOGGER = Logger.getLogger(FindAllDOM.class);

	@Override
	public String execute(HttpServletRequest request) {
		

		String page = null;
		List<SyntheticJewel> listSJ = new ArrayList<SyntheticJewel>();
		List<SyntheticIndustry> listSI = new ArrayList<SyntheticIndustry>();
		List<Gem> tempList = new ArrayList<Gem>();
try{
	    String prefix = request.getServletContext().getRealPath("/");
		String XMLPath =  prefix + "files\\gems.xml";
		GemsBuilderFactory factory = GemsBuilderFactory.getInstance();
	 	factory.createGemsParser("DOM").buildGems(XMLPath);
	 	tempList = GemDOMParser.getList();
		for (Gem gem : tempList) {
			if (gem instanceof SyntheticJewel) {
				listSJ.add((SyntheticJewel) gem);
			}
		}
		for (Gem gem : tempList) {
			if (gem instanceof SyntheticIndustry) {
				listSI.add((SyntheticIndustry) gem);
			}
		}
		}catch (LogicException e) {
    	LOGGER.error("Wrong data in XML file");
		}
		if (listSJ != null) {
			request.setAttribute("result1", listSJ);
		}
		if (listSI != null) {
			request.setAttribute("result2", listSI);
			request.setAttribute("info", "DOM");
		}
		page = "/jsp/main.jsp";
		return page;
	}

}
