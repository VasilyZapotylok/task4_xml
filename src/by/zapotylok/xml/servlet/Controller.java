package by.zapotylok.xml.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import by.zapotylok.xml.command.ActionCommand;
import by.zapotylok.xml.command.ActionFactory;


@WebServlet("/controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 6633399994062236002L;


protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    processRequest(request, response);
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    processRequest(request, response);
  }

  private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String page = null;
    ActionFactory client = new ActionFactory();
    ActionCommand command = client.defineCommand(request);
    page = command.execute(request);
    if (page != null) {
      RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
      dispatcher.forward(request, response);
    } else {
      page = "/jsp/choice.jsp";
    }
  }
}