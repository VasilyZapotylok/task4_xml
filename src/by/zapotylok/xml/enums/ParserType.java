package by.zapotylok.xml.enums;

public enum ParserType {
	
	DOM, SAX, STAX

}
