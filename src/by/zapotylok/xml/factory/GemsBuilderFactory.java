package by.zapotylok.xml.factory;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import by.zapotylok.xml.builder.AbstractParserBuilder;
import by.zapotylok.xml.builder.DOMParserBuilder;
import by.zapotylok.xml.builder.SAXParserBuilder;
import by.zapotylok.xml.builder.STAXParserBuilder;
import by.zapotylok.xml.enums.ParserType;
import by.zapotylok.xml.exception.LogicException;


public class GemsBuilderFactory {


	private static AtomicBoolean isNull = new AtomicBoolean(true);
	private static ReentrantLock lock = new ReentrantLock();
	private static GemsBuilderFactory instance;

	public static GemsBuilderFactory getInstance() {
		if (isNull.get()) {
			lock.lock();
			try {
				if (isNull.get()) {
					instance = new GemsBuilderFactory();
					isNull.set(false);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	public AbstractParserBuilder createGemsParser(String typeParser) throws LogicException {
		try {
			ParserType type = ParserType.valueOf(typeParser.toUpperCase());
			switch (type) {
			case DOM:
				return new DOMParserBuilder();
			case STAX:
				return new STAXParserBuilder();
			case SAX:
				return new SAXParserBuilder();
			default:
				throw new LogicException();
			}
		} catch (IllegalArgumentException e) {
			throw new LogicException();
		}
	}
}
