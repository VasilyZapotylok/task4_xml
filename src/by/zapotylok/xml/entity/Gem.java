package by.zapotylok.xml.entity;

import by.zapotylok.xml.exception.LogicException;

public class Gem {
	
	private String id;
	private String name;
	private String preciousness;
	private String origin;
	private String color;
	private int value;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPreciousness() {
		return preciousness;
	}
	public void setPreciousness(String preciousness) {
		this.preciousness = preciousness;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) throws LogicException {
		if ((value >= 1) & (value <= 1000)){
		this.value = value;
		}
		else {
			throw new LogicException("Value must be in range 1...1000");
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((preciousness == null) ? 0 : preciousness.hashCode());
		result = prime * result + value;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gem other = (Gem) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (preciousness == null) {
			if (other.preciousness != null)
				return false;
		} else if (!preciousness.equals(other.preciousness))
			return false;
		if (value != other.value)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Gem [id=" + id + ", name=" + name + ", preciousness=" + preciousness + ", origin=" + origin + ", color="
				+ color + ", value=" + value + "]";
	}
		
	

}
