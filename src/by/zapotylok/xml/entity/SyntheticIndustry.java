package by.zapotylok.xml.entity;



public class SyntheticIndustry extends Gem{
	
	private String hardness;

	public String getHardness() {
		return hardness;
	}

	public void setHardness(String hardness) {
		this.hardness = hardness;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((hardness == null) ? 0 : hardness.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntheticIndustry other = (SyntheticIndustry) obj;
		if (hardness == null) {
			if (other.hardness != null)
				return false;
		} else if (!hardness.equals(other.hardness))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SyntheticIndustry [hardness=" + hardness + ", getId()=" + getId() + ", getName()=" + getName()
				+ ", getPreciousness()=" + getPreciousness() + ", getOrigin()=" + getOrigin() + ", getColor()="
				+ getColor() + ", getValue()=" + getValue() + "]";
	}

	
	
	
	

}
