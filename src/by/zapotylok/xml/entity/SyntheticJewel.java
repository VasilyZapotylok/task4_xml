package by.zapotylok.xml.entity;


import by.zapotylok.xml.exception.LogicException;

public class SyntheticJewel extends Gem {
	
	private String form;
	private int smoothness;
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public int getSmoothness() {
		return smoothness;
	}
	public void setSmoothness(int smoothness) throws LogicException {
		if ((smoothness >= 1) & (smoothness <= 100)){
		this.smoothness = smoothness;
		}
		else {
			throw new LogicException("Smoothness must be in range 1...100");
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((form == null) ? 0 : form.hashCode());
		result = prime * result + smoothness;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyntheticJewel other = (SyntheticJewel) obj;
		if (form == null) {
			if (other.form != null)
				return false;
		} else if (!form.equals(other.form))
			return false;
		if (smoothness != other.smoothness)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SyntheticJewel [form=" + form + ", smoothness=" + smoothness + ", getId()=" + getId() + ", getName()="
				+ getName() + ", getPreciousness()=" + getPreciousness() + ", getOrigin()=" + getOrigin()
				+ ", getColor()=" + getColor() + ", getValue()=" + getValue() + "]";
	}
	
	

	

}
