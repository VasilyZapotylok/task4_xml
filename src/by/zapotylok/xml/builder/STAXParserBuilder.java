package by.zapotylok.xml.builder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import by.zapotylok.xml.exception.FileException;
import by.zapotylok.xml.parser.GemSTAXParser;



public class STAXParserBuilder extends AbstractParserBuilder {
	private static final Logger LOGGER = Logger.getLogger(STAXParserBuilder.class);

	public void buildGems(String path) {
		try (FileInputStream inStream = new FileInputStream(new File(path))) {
			XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(inStream);
			GemSTAXParser parser = new GemSTAXParser();
			parser.buildGemsList(reader);
			LOGGER.info("STAX Parser done");
			parser.showGems();
		} catch (IOException | XMLStreamException | FactoryConfigurationError|FileException e) {
			LOGGER.error(e);
		}
	}

}
