package by.zapotylok.xml.builder;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;


import by.zapotylok.xml.parser.GemSAXParser;

public class SAXParserBuilder extends AbstractParserBuilder {
	
	private static final Logger LOGGER = Logger.getLogger(SAXParserBuilder.class);
	static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
	static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
	private GemSAXParser parser = new GemSAXParser();
	
	public void buildGems(String path)  {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setNamespaceAware(true);
			factory.setValidating(true);
			SAXParser saxParser = factory.newSAXParser();
			saxParser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
			File file = new File(path);
			saxParser.parse(file, parser);
			LOGGER.info("SAX Parser done");
			parser.showGems();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			LOGGER.error(e);
		}
	}

}
