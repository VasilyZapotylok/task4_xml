package by.zapotylok.xml.builder;

import java.io.File;

import org.apache.log4j.Logger;

import by.zapotylok.xml.parser.GemDOMParser;


public class DOMParserBuilder extends AbstractParserBuilder {
	
	private static final Logger LOGGER = Logger.getLogger(DOMParserBuilder.class);
	private GemDOMParser parser = new GemDOMParser();

	
	public void buildGems(String path) {
		File file = new File(path);
		parser.buildListGems(file);
		LOGGER.info("DOM Pasrer done!");
		parser.showGems();
		
	}




}
