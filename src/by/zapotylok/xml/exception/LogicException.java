package by.zapotylok.xml.exception;

public class LogicException extends Exception {

	private static final long serialVersionUID = 798178093620656352L;

	public LogicException() {
		super();
	}

	public LogicException(String message, Throwable cause) {
		super(message, cause);
	}

	public LogicException(String message) {
		super(message);
	}

	public LogicException(Throwable cause) {
		super(cause);
	}

}
