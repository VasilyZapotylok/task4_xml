package by.zapotylok.xml.parser;

import java.util.ArrayList;
import java.util.EnumSet;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import by.zapotylok.xml.entity.Gem;
import by.zapotylok.xml.entity.SyntheticIndustry;
import by.zapotylok.xml.entity.SyntheticJewel;
import by.zapotylok.xml.enums.GemsElement;
import by.zapotylok.xml.exception.FileException;
import by.zapotylok.xml.exception.LogicException;

public class GemSAXParser extends DefaultHandler {

	private static final Logger LOGGER = Logger.getLogger(GemSAXParser.class);
	private String gemname;
	private String preciousness;
	private String origin;
	private String color;
	private int value;
	private SyntheticJewel syntheticJewel;
	private SyntheticIndustry syntheticIndustry;
	private GemsElement gemsElement;
	private static ArrayList<Gem> gems;
	private EnumSet<GemsElement> withText;

	public GemSAXParser() {
		gems = new ArrayList<>();
		withText = EnumSet.range(GemsElement.NAME, GemsElement.HARDNESS);
	}

	public void startElement(String uri, String name, String qname, Attributes attrs) {
		switch (name) {
		case "synthetic-jewel":
			syntheticJewel = new SyntheticJewel();
			syntheticJewel.setId(attrs.getValue("id"));
			break;
		case "synthetic-industry":
			syntheticIndustry = new SyntheticIndustry();
			syntheticIndustry.setId(attrs.getValue("id"));
		default:
			GemsElement temp = GemsElement.valueOf(name.replace("-", "_").toUpperCase());
			if (withText.contains(temp)) {
				gemsElement = temp;
			}
			break;
		}
	}

	public void characters(char[] buf, int offset, int len) {
		String s = new String(buf, offset, len).trim();
		try {
			if (gemsElement != null) {
				switch (gemsElement) {
				case NAME:
					gemname = s;
					break;
				case PRECIOUSNESS:
					preciousness = s;
					break;
				case ORIGIN:
					origin = s;
					break;
				case VISUAL_PARAMETER:
					color = s;
					break;
				case VALUE:
					value = Integer.parseInt(s);
					break;
				case FORM:
					syntheticJewel.setForm(s);
					break;
				case SMOOTHNESS:
					syntheticJewel.setSmoothness(Integer.parseInt(s));
					break;
				case HARDNESS:
					syntheticIndustry.setHardness(s);
					break;
				default:
					throw new FileException();
				}
			}
		} catch (LogicException | FileException e) {
			LOGGER.error("Mistake in file or wrong data");
		}
	}

	public void endElement(String uri, String name, String qname) {
		try {
			switch (name) {
			case "synthetic-jewel":
				syntheticJewel.setName(gemname);
				syntheticJewel.setPreciousness(preciousness);
				syntheticJewel.setOrigin(origin);
				syntheticJewel.setColor(color);
				syntheticJewel.setValue(value);
				gems.add(syntheticJewel);
				syntheticJewel = null;
				break;
			case "synthetic-industry":
				syntheticIndustry.setName(gemname);
				syntheticIndustry.setPreciousness(preciousness);
				syntheticIndustry.setOrigin(origin);
				syntheticIndustry.setColor(color);
				syntheticIndustry.setValue(value);
				gems.add(syntheticIndustry);
				syntheticIndustry = null;
			}
		} catch (LogicException e) {
			LOGGER.error("Wrong data");
		}
	}

	public void showGems() {
		for (Gem gem : gems) {
			LOGGER.info(gem);
		}
	}
	public static ArrayList<Gem> getList(){
		return gems;
	}
}
