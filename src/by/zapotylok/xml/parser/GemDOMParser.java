package by.zapotylok.xml.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.zapotylok.xml.entity.Gem;
import by.zapotylok.xml.entity.SyntheticIndustry;
import by.zapotylok.xml.entity.SyntheticJewel;
import by.zapotylok.xml.exception.LogicException;

public class GemDOMParser {
	private static final Logger LOGGER = Logger.getLogger(GemDOMParser.class);
	private static ArrayList<Gem> gems = new ArrayList<>();
	private DocumentBuilder documentBuilder;

	public void buildListGems(File file) {
		try {
			initDocumentBuilder();
			Document document = documentBuilder.parse(file);
			Element root = document.getDocumentElement();
			NodeList syntheticJewelList = root.getElementsByTagName("synthetic-jewel");
			NodeList syntheticIndustryList = root.getElementsByTagName("synthetic-industry");
			for (int i = 0; i < syntheticJewelList.getLength(); i++) {
				Element syntheticJewelElement = (Element) syntheticJewelList.item(i);
				SyntheticJewel syntheticJewel = buildSyntheticJewel(syntheticJewelElement);
				gems.add(syntheticJewel);
			}
			for (int i = 0; i < syntheticIndustryList.getLength(); i++) {
				Element syntheticIndustryElement = (Element) syntheticJewelList.item(i);
				SyntheticIndustry syntheticIndustry = buildSyntheticIndustry(syntheticIndustryElement);
				gems.add(syntheticIndustry);
			}
		} catch (LogicException | ParserConfigurationException | SAXException | IOException e) {
			LOGGER.error("Can't parse XML file");
		}
	}

	private SyntheticJewel buildSyntheticJewel(Element syntheticJewelElement) throws LogicException {
		SyntheticJewel syntheticJewel = new SyntheticJewel();

		syntheticJewel.setId(syntheticJewelElement.getAttribute("id"));
		syntheticJewel.setName(getElementTextContent(syntheticJewelElement, "name"));
		syntheticJewel.setPreciousness(getElementTextContent(syntheticJewelElement, "preciousness"));
		syntheticJewel.setOrigin(getElementTextContent(syntheticJewelElement, "origin"));
		syntheticJewel.setColor(getElementTextContent(syntheticJewelElement, "visual-parameter"));
		int value = Integer.parseInt(getElementTextContent(syntheticJewelElement, "value"));
		syntheticJewel.setValue(value);
		syntheticJewel.setForm(getElementTextContent(syntheticJewelElement, "form"));
		int smoothness = Integer.parseInt(getElementTextContent(syntheticJewelElement, "smoothness"));
		syntheticJewel.setSmoothness(smoothness);

		return syntheticJewel;
	}

	private SyntheticIndustry buildSyntheticIndustry(Element syntheticIndustryElement) throws LogicException {
		SyntheticIndustry syntheticIndustry = new SyntheticIndustry();

		syntheticIndustry.setId(syntheticIndustryElement.getAttribute("id"));
		syntheticIndustry.setName(getElementTextContent(syntheticIndustryElement, "name"));
		syntheticIndustry.setPreciousness(getElementTextContent(syntheticIndustryElement, "preciousness"));
		syntheticIndustry.setOrigin(getElementTextContent(syntheticIndustryElement, "origin"));
		syntheticIndustry.setColor(getElementTextContent(syntheticIndustryElement, "visual-parameter"));
		int value = Integer.parseInt(getElementTextContent(syntheticIndustryElement, "value"));
		syntheticIndustry.setValue(value);
		syntheticIndustry.setHardness(getElementTextContent(syntheticIndustryElement, "hardness"));
		return syntheticIndustry;
	}

	private String getElementTextContent(Element element, String elementName) {
		String text = null;
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		if (node != null) {
			text = node.getTextContent();
		}
		return text;
	}

	private void initDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		documentBuilder = factory.newDocumentBuilder();
	}

	public void showGems() {
		for (Gem gem : gems) {
			LOGGER.info(gem);
		}
	}
	public static ArrayList<Gem> getList(){
		return gems;
	}
}
