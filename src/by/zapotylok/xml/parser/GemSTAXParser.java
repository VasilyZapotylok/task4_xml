package by.zapotylok.xml.parser;

import java.util.ArrayList;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import by.zapotylok.xml.entity.Gem;
import by.zapotylok.xml.entity.SyntheticIndustry;
import by.zapotylok.xml.entity.SyntheticJewel;
import by.zapotylok.xml.enums.GemsElement;
import by.zapotylok.xml.exception.FileException;
import by.zapotylok.xml.exception.LogicException;

public class GemSTAXParser implements XMLStreamConstants {

	private static final Logger LOGGER = Logger.getLogger(GemSTAXParser.class);
	private SyntheticJewel syntheticJewel;
	private SyntheticIndustry syntheticIndustry;
	private static ArrayList<Gem> gems = new ArrayList<>();

	public void buildGemsList(XMLStreamReader reader) throws FileException  {
		try {
			String name;
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					switch (GemsElement.valueOf(name.replace("-", "_").toUpperCase())) {
					case SYNTHETIC_JEWEL:
						syntheticJewel = buildSyntheticJewel(reader);
						gems.add(syntheticJewel);
						break;
					case SYNTHETIC_INDUSTRY:
						syntheticIndustry = buildSyntheticIndustry(reader);
						gems.add(syntheticIndustry);
					default:
						
					}
				}
			}
		} catch (XMLStreamException | LogicException e) {
			LOGGER.error("Can't read file");
		}
	}

	private SyntheticJewel buildSyntheticJewel(XMLStreamReader reader)
			throws LogicException, XMLStreamException, FileException {
		SyntheticJewel syntheticJewel = new SyntheticJewel();
		syntheticJewel.setId(reader.getAttributeValue(null, "id"));
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (GemsElement.valueOf(name.replace("-", "_").toUpperCase())) {
				case NAME:
					syntheticJewel.setName(getXMLText(reader));
					break;
				case PRECIOUSNESS:
					syntheticJewel.setPreciousness(getXMLText(reader));
					break;
				case ORIGIN:
					syntheticJewel.setOrigin(getXMLText(reader));
					break;
				case VISUAL_PARAMETER:
					syntheticJewel.setColor(getXMLText(reader));
					break;
				case VALUE:
					syntheticJewel.setValue(Integer.parseInt(getXMLText(reader)));
					break;
				case FORM:
					syntheticJewel.setForm(getXMLText(reader));
					break;
				case SMOOTHNESS:
					syntheticJewel.setSmoothness(Integer.parseInt(getXMLText(reader)));
					break;
				default:
					throw new FileException();
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (GemsElement.valueOf(name.replace("-", "_").toUpperCase()) == GemsElement.SYNTHETIC_JEWEL) {
					return syntheticJewel;
				}
			}
		}
		return syntheticJewel;
	}

	private SyntheticIndustry buildSyntheticIndustry(XMLStreamReader reader)
			throws LogicException, XMLStreamException, FileException {
		SyntheticIndustry syntheticIndustry = new SyntheticIndustry();
		syntheticIndustry.setId(reader.getAttributeValue(null, "id"));
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				switch (GemsElement.valueOf(name.replace("-", "_").toUpperCase())) {
				case NAME:
					syntheticIndustry.setName(getXMLText(reader));
					break;
				case PRECIOUSNESS:
					syntheticIndustry.setPreciousness(getXMLText(reader));
					break;
				case ORIGIN:
					syntheticIndustry.setOrigin(getXMLText(reader));
					break;
				case VISUAL_PARAMETER:
					syntheticIndustry.setColor(getXMLText(reader));
					break;
				case VALUE:
					syntheticIndustry.setValue(Integer.parseInt(getXMLText(reader)));
					break;
				case HARDNESS:
					syntheticIndustry.setHardness(getXMLText(reader));
					break;
				default:
					throw new FileException();
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (GemsElement.valueOf(name.replace("-", "_").toUpperCase()) == GemsElement.SYNTHETIC_INDUSTRY) {
					return syntheticIndustry;
				}
			}
		}
		return syntheticIndustry;
	}

	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}
		return text;
	}

	public void showGems() {
		for (Gem gem : gems) {
			LOGGER.info(gem);
		}
	}
	public static ArrayList<Gem> getList(){
		return gems;
	}
}
