<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<body>
Результат работы парсера ${info}
	<table border="1">
		<tr>
			<th>id</th>
			<th>name</th>
			<th>preciousness</th>
			<th>origin</th>
			<th>color</th>
			<th>value</th>
			<th>form</th>
			<th>smoothness</th>
		</tr>
		<c:forEach items="${result1}" var="current">
			<tr>
				<td><c:out value="${current.id}" /></td>
				<td><c:out value="${current.name}" /></td>
				<td><c:out value="${current.preciousness}" /></td>
				<td><c:out value="${current.origin}" /></td>
				<td><c:out value="${current.color}" /></td>
				<td><c:out value="${current.value}" /></td>
				<td><c:out value="${current.form}" /></td>
				<td><c:out value="${current.smoothness}" /></td>
			</tr>
		</c:forEach>
	</table>
	<table border="1">
		<tr>
			<th>id</th>
			<th>name</th>
			<th>preciousness</th>
			<th>origin</th>
			<th>color</th>
			<th>value</th>
			<th>hardness</th>
		</tr>
		<c:forEach items="${result2}" var="current">
			<tr>
				<td><c:out value="${current.id}" /></td>
				<td><c:out value="${current.name}" /></td>
				<td><c:out value="${current.preciousness}" /></td>
				<td><c:out value="${current.origin}" /></td>
				<td><c:out value="${current.color}" /></td>
				<td><c:out value="${current.value}" /></td>
				<td><c:out value="${current.hardness}" /></td>
			</tr>
		</c:forEach>
	</table>
	<a href="index.jsp">return</a>
</body>
</html>